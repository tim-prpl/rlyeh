#!/bin/sh
[ -f /etc/environment ] && source /etc/environment
ulimit -c ${ULIMIT_CONFIGURATION:-0}
name="rlyeh"

kill_pid()
{
    PIDFILE=${1}
    if [ -f "${PIDFILE}" ]; then
        kill `cat ${PIDFILE}`
    else
        echo "Can't find ${PIDFILE}. Killing all ${name} processes"
	killall ${name}
    fi
}

case $1 in
    start|boot)
        echo "Starting ${name}"
        env LD_LIBRARY_PATH="/opt/prplos/usr/lib/" $name -D
        ;;
    stop|shutdown)
        kill_pid /var/run/${name}.pid
        ;;
    debuginfo)
	ubus-cli "Rlyeh.?"
        ;;
    restart)
        $0 stop
        $0 start
        ;;
    log)
	echo "TODO log ${name}"
	;;
    *)
        echo "Usage : $0 [start|boot|stop|shutdown|debuginfo|log]"
        ;;
esac
