%populate {
    object Rlyeh {
        parameter OnboardingFile = "LCM_STORAGE_LOCATION/rlyeh_onboarded";
        parameter ImageLocation = "LCM_STORAGE_LOCATION/rlyeh/images";
        parameter ROImageLocation = "/usr/rlyeh/images";
        parameter StorageLocation = "LCM_STORAGE_LOCATION/rlyeh/blobs";
        parameter ROStorageLocation = "/usr/rlyeh/blobs";
        parameter SignatureVerification = 1;
        parameter CertificateVerification = 1;
        parameter RemainingDiskSpaceBytes = 1000001;
    }
}
