/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <dirent.h>

#include <debug/sahtrace.h>
#include <yajl/yajl_gen.h>
#include <yajl/yajl_parse.h>
#include <amxc/amxc_string.h>
#include <amxc/amxc_variant_type.h>

#include <rlyeh/rlyeh_defines.h>
#include <rlyeh/rlyeh_image_info.h>
#include <rlyeh/rlyeh_utils.h>
#include <rlyeh/rlyeh_copy.h>
#include <rlyeh/rlyeh_remove.h>
#include <rlyeh/rlyeh_status.h>
#include <rlyeh/rlyeh_sync.h>
#include <rlyeh/rlyeh_image_index_generation.h>

#include "rlyeh.h"
#include "rlyeh_dm.h"
#include "rlyeh_dm_notif.h"
#include "rlyeh_worker_func.h"
#include "rlyeh_worker_signals.h"
#include "rlyeh_worker.h"

#define ME "rlyeh_functions"

#define NOTIF_WORKER_FAILED(obj, command_id, type, cmd, ...) \
    rlyeh_worker_emit_error(obj, command_id, type, cmd, __VA_ARGS__)

static int rlyeh_worker_emit_error(amxd_object_t* obj, const char* command_id, tr181_fault_type_t type, const char* cmd, const char* reason, ...) {
    int retval = -1;
    va_list args;
    amxc_var_t data;
    amxc_string_t r;
    amxc_var_init(&data);
    amxc_string_init(&r, 0);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &data, RLYEH_COMMAND, cmd);
    amxc_var_add_key(cstring_t, &data, RLYEH_NOTIF_COMMAND_ID, command_id);
    amxc_var_add_key(int32_t, &data, RLYEH_NOTIF_ERROR_TYPE, type);
    if(!string_is_empty(reason)) {
        va_start(args, reason);
        amxc_string_vsetf(&r, reason, args);
        va_end(args);
        amxc_var_add_key(cstring_t, &data, RLYEH_NOTIF_ERROR_REASON, amxc_string_get(&r, 0));
    }
    SAH_TRACEZ_ERROR(ME, "Emit error CID [%s] Fault [%s], Cmd [%s], Reason [%s]",
                     command_id, tr181_fault_type_to_string(type), cmd, r.buffer);
    worker_write_notification(obj, &data, rlyeh_cmd_failed);

    amxc_string_clean(&r);
    amxc_var_clean(&data);
    return retval;
}

static int remove_unlisted_imagespec(const char* storage_location, const char* image_location, amxd_object_t* images) {
    amxc_llist_t llimages;
    amxc_llist_init(&llimages);
    amxd_object_for_each(instance, it, images) {
        amxd_object_t* instance = amxc_container_of(it, amxd_object_t, it);
        char* name = amxd_object_get_value(cstring_t, instance, RLYEH_DM_IMAGE_NAME, NULL);
        char* version = amxd_object_get_value(cstring_t, instance, RLYEH_DM_IMAGE_VERSION, NULL);
        amxc_string_t str;
        amxc_string_init(&str, 0);
        amxc_string_setf(&str, "%s:%s", name, version);
        amxc_llist_add_string(&llimages, amxc_string_get(&str, 0));
        amxc_string_clean(&str);
        free(name);
        free(version);
    }

    int ret = rlyeh_remove_unlisted_imagespec(storage_location, image_location, &llimages);
    amxc_llist_clean(&llimages, amxc_string_list_it_free);
    return ret;
}

// This will remove the actual image, but the caller still needs to call 'amxd_object_delete(&instance)'
// to remove from dm
static int remove_image(const char* storage_location, const char* image_location, amxd_object_t* instance) {
    int res = 0;
    char* name = amxd_object_get_value(cstring_t, instance, RLYEH_DM_IMAGE_NAME, NULL);
    char* duid = amxd_object_get_value(cstring_t, instance, RLYEH_DM_IMAGE_DUID, NULL);
    char* uri = amxd_object_get_value(cstring_t, instance, RLYEH_DM_IMAGE_URI, NULL);
    char* version = amxd_object_get_value(cstring_t, instance, RLYEH_DM_IMAGE_VERSION, NULL);
    amxd_object_t* image = NULL;
    if(name && version) {
        SAH_TRACEZ_INFO(ME, "removing image '%s' [%s:%s]", duid, name, version);
        // Check if there are other images with another duid and the same name:version
        image = amxd_dm_findf(rlyeh_get_dm(),
                              RLYEH_DM_IMAGES ".[" \
                              RLYEH_DM_IMAGE_DUID " != \"%s\" && " \
                              RLYEH_DM_IMAGE_NAME " == \"%s\" && " \
                              RLYEH_DM_IMAGE_VERSION " == \"%s\"]",
                              duid,
                              name,
                              version);
    }

    if(image) {
        // Other images are still using name:version, so don't delete
        SAH_TRACEZ_WARNING(ME, "Will not let Rlyeh remove [%s:%s]", name, version);
        goto exit;
    }
    // Last image with that name:version, Rlyeh can delete it
    rlyeh_remove_data_t remove_param;
    rlyeh_remove_data_init(&remove_param);

    remove_param.storageLocation = strdup(storage_location);
    remove_param.imageLocation = strdup(image_location);
    remove_param.name = strdup(name);
    remove_param.version = strdup(version);
    res = rlyeh_remove(&remove_param);

    rlyeh_remove_data_clean(&remove_param);

exit:
    free(name);
    free(duid);
    free(version);
    free(uri);

    return res;
}

static rlyeh_status_t pull_image(const char* storage_location, const char* storage_location_ro, const char* image_location, UNUSED const char* image_location_ro, bool signature_verification, bool certificate_verification, amxc_var_t* args, char* err_msg) {
    rlyeh_status_t res = RLYEH_ERROR_UNKNOWN_ERROR;
    const char* uri = GET_CHAR(args, RLYEH_CMD_PULL_URI);
    const char* duid = GET_CHAR(args, RLYEH_CMD_PULL_DUID);
    const char* username = GET_CHAR(args, RLYEH_CMD_PULL_USERNAME);
    const char* password = GET_CHAR(args, RLYEH_CMD_PULL_PASSWORD);
    const char* signature_url_overload = GET_CHAR(args, RLYEH_CMD_PULL_SIGNATURE_URL);
    amxc_string_t user;
    amxc_string_t destination;
    rlyeh_image_parameters_t uri_elements;
    amxc_string_init(&user, 64);
    amxc_string_init(&destination, 64);
    rlyeh_image_parameters_init(&uri_elements);

    rlyeh_parse_uri(uri, &uri_elements);
    amxc_string_setf(&destination, "oci:%s/%s:%s", image_location, uri_elements.image_name, uri_elements.version);

    rlyeh_copy_data_t copy_param;
    rlyeh_copy_data_init(&copy_param);
    copy_param.dest_shared_blob_dir = strdup(storage_location);
    copy_param.destination = strdup(destination.buffer);
    copy_param.source = strdup(uri);
    copy_param.duid = strdup(duid);
    copy_param.sv = signature_verification;
    copy_param.cv = certificate_verification;
    if(storage_location_ro && (!string_is_empty(storage_location_ro))) {
        copy_param.ro_shared_blob_dir = strdup(storage_location_ro);
    }
    if(signature_verification && (!string_is_empty(signature_url_overload))) {
        copy_param.signature_url_overload = strdup(signature_url_overload);
    }

    if(!string_is_empty(username) && !string_is_empty(password)) {
        amxc_string_setf(&user, "%s:%s", username, password);
        copy_param.user = strdup(user.buffer);
    }

    res = rlyeh_copy(&copy_param, err_msg);

    amxc_string_clean(&user);
    amxc_string_clean(&destination);
    rlyeh_image_parameters_clean(&uri_elements);
    rlyeh_copy_data_clean(&copy_param);

    return res;
}

tr181_fault_type_t rlyeh_exec_pull(amxd_object_t* obj, amxc_var_t* args) {
    tr181_fault_type_t status = tr181_fault_request_denied;
    amxd_object_t* rlyeh = NULL;
    char* storage_location = NULL;
    char* image_location = NULL;
    char* ro_storage_location = NULL;
    char* ro_image_location = NULL;
    char err_msg[RLYEH_ERR_MSG_LEN] = "";
    int64_t remainingdiskspacebytes = 0;

    when_null(obj, error);
    when_null(args, error);
    rlyeh = amxd_dm_get_object(rlyeh_get_dm(), RLYEH_DM);
    when_null(rlyeh, error);

    const char* command_id = GET_CHAR(args, RLYEH_NOTIF_COMMAND_ID);

    ro_storage_location = amxd_object_get_value(cstring_t, rlyeh, RLYEH_STORAGE_LOCATION_RO, NULL);
    storage_location = amxd_object_get_value(cstring_t, rlyeh, RLYEH_STORAGE_LOCATION, NULL);
    if(string_is_empty(storage_location)) {
        NOTIF_WORKER_FAILED(obj, command_id, status, RLYEH_CMD_PULL, "storage location is undefined");
        goto notify;
    }
    ro_image_location = amxd_object_get_value(cstring_t, rlyeh, RLYEH_IMAGE_LOCATION_RO, NULL);
    image_location = amxd_object_get_value(cstring_t, rlyeh, RLYEH_IMAGE_LOCATION, NULL);
    if(string_is_empty(image_location)) {
        NOTIF_WORKER_FAILED(obj, command_id, status, RLYEH_CMD_PULL, "image location is undefined");
        goto notify;
    }
    remainingdiskspacebytes = amxd_object_get_value(int64_t, rlyeh, RLYEH_REMAINING_DISKSPACE_BYTES, NULL);
    if(remainingdiskspacebytes > 0) {
        int64_t imagespacefree = get_free_disk_space(image_location);
        if(imagespacefree < remainingdiskspacebytes) {
            SAH_TRACEZ_ERROR(ME, "'%s' does not have %" PRId64 " disk space available", image_location, remainingdiskspacebytes);
            NOTIF_WORKER_FAILED(obj, command_id, status, RLYEH_CMD_PULL, "'%s' does not have %" PRId64 " disk space available", image_location, remainingdiskspacebytes);
            goto notify;
        }
    } else {
        SAH_TRACEZ_ERROR(ME, "Remaining Disk Space will not be checked");
    }

    const char* uri = GET_CHAR(args, RLYEH_CMD_PULL_URI);
    const char* duid = GET_CHAR(args, RLYEH_CMD_PULL_DUID);
    bool signature_verification = amxd_object_get_value(bool, rlyeh, RLYEH_SIGNATURE_VERIFICATION, NULL);
    bool certificate_verification = amxd_object_get_value(bool, rlyeh, RLYEH_CERTIFICATE_VERIFICATION, NULL);
    rlyeh_status_t res = RLYEH_ERROR_UNKNOWN_ERROR;
    SAH_TRACEZ_INFO(ME, "Exec pull uri %s", uri);

    if(string_is_empty(uri)) {
        status = tr181_fault_invalid_arguments;
        NOTIF_WORKER_FAILED(obj, command_id, status, RLYEH_CMD_PULL, "URI is empty");
        goto notify;
    }
    if(string_is_empty(duid)) {
        status = tr181_fault_invalid_arguments;
        NOTIF_WORKER_FAILED(obj, command_id, status, RLYEH_CMD_PULL, "DUID is empty");
        goto notify;
    }

    res = pull_image(storage_location, ro_storage_location, image_location, ro_image_location, signature_verification, certificate_verification, args, err_msg);
    switch(res) {
    case RLYEH_NO_ERROR:
    case RLYEH_IMAGE_ALREADY_EXISTS:
        status = tr181_fault_ok;
        break;
    case RLYEH_ERROR_INVALID_ARGUMENTS:
        status = tr181_fault_invalid_arguments;
        NOTIF_WORKER_FAILED(obj, command_id, status, RLYEH_CMD_PULL, "Pull image [%s] failed [%s]", uri, err_msg);
        break;
    case RLYEH_ERROR_DISK_SPACE:
        status = tr181_fault_system_resources_exceeded;
        NOTIF_WORKER_FAILED(obj, command_id, status, RLYEH_CMD_PULL, "Pull image [%s] failed [%s]", uri, err_msg);
        break;
    case RLYEH_ERROR_SIZE_CHECK:
    case RLYEH_ERROR_INVALID_CONTENT:
    case RLYEH_ERROR_INVALID_SIGNATURE:
    case RLYEH_ERROR_CERTIFICATE_CHECK_FAILED:
    case RLYEH_ERROR_CURL_OPERATION_TIMEDOUT:
    case RLYEH_ERROR_CURL_COULDNT_RESOLVE_HOST:
    case RLYEH_ERROR_DOWNLOAD_FAILED:
    case RLYEH_ERROR_UNKNOWN_ERROR:
    default:
        status = tr181_fault_request_denied;
        NOTIF_WORKER_FAILED(obj, command_id, status, RLYEH_CMD_PULL, "Pull image [%s] failed [%s]", uri, err_msg);
        break;
    }

notify:
    // Notify the main thread to do DM trans and external notifications.
    amxc_var_add_new_key_int32_t(args, RLYEH_CMD_RES, (int32_t) status);
    worker_write_notification(obj, args, rlyeh_pull_cb);
    goto exit;
error:
    rlyeh_worker_signal_task_complete();
exit:
    free(image_location);
    free(storage_location);
    return status;
}

tr181_fault_type_t rlyeh_exec_remove(amxd_object_t* obj, amxc_var_t* args) {
    tr181_fault_type_t status = tr181_fault_invalid_arguments;
    amxd_object_t* image = NULL;
    amxd_object_t* rlyeh = NULL;
    char* image_location = NULL;
    char* image_name = NULL;

    when_null(obj, error);
    when_null(args, error);
    rlyeh = amxd_dm_get_object(rlyeh_get_dm(), RLYEH_DM);
    when_null(rlyeh, error);
    image_location = amxd_object_get_value(cstring_t, rlyeh, RLYEH_IMAGE_LOCATION, NULL);
    when_null(image_location, error);

    const char* command_id = GET_CHAR(args, RLYEH_NOTIF_COMMAND_ID);
    const char* duid = GET_CHAR(args, RLYEH_CMD_PULL_DUID);
    const char* version = GET_CHAR(args, RLYEH_DM_IMAGE_VERSION);
    if(string_is_empty(duid)) {
        status = tr181_fault_invalid_arguments;
        NOTIF_WORKER_FAILED(obj, command_id, status, RLYEH_CMD_REMOVE, RLYEH_CMD_PULL_DUID " is empty");
        goto error;
    }
    if(string_is_empty(version)) {
        status = tr181_fault_invalid_arguments;
        NOTIF_WORKER_FAILED(obj, command_id, status, RLYEH_CMD_REMOVE, RLYEH_DM_IMAGE_VERSION " is empty");
        goto error;
    }

    image = amxd_dm_findf(rlyeh_get_dm(), RLYEH_DM_IMAGES ".[" RLYEH_DM_IMAGE_DUID " == \"%s\" && " RLYEH_DM_IMAGE_VERSION " == '%s'].", duid, version);
    if(image == NULL) {
        status = tr181_fault_request_denied;
        NOTIF_WORKER_FAILED(obj,
                            command_id,
                            status,
                            RLYEH_CMD_REMOVE,
                            "Could't find image with " RLYEH_DM_IMAGE_DUID " (%s) - " RLYEH_DM_IMAGE_VERSION " (%s)",
                            duid,
                            version);
        goto error;
    }

    image_name = amxd_object_get_value(cstring_t, image, RLYEH_DM_IMAGE_NAME, NULL);
    rlyeh_update_index_annotations(image_location, image_name, duid, version, RLYEH_DM_IMAGE_MARK_RM, "1");
    SAH_TRACEZ_INFO(ME, "Image marked for remove in image spec");

    status = tr181_fault_ok;
    // Notify the main thread to do DM trans and external notifications.
    worker_write_notification(obj, args, rlyeh_remove_cb);
    goto exit;

error:
    rlyeh_worker_signal_task_complete();
exit:
    free(image_location);
    free(image_name);
    return status;
}

tr181_fault_type_t rlyeh_exec_gc(amxd_object_t* obj, amxc_var_t* args) {
    int res;
    tr181_fault_type_t status = tr181_fault_ok;
    amxd_object_t* rlyeh = NULL;
    char* storage_location = NULL;
    char* image_location = NULL;
    SAH_TRACEZ_INFO(ME, "executing " RLYEH_CMD_GC);

    when_null(obj, error);
    when_null(args, error);
    rlyeh = amxd_dm_get_object(rlyeh_get_dm(), RLYEH_DM);
    when_null(rlyeh, error);
    storage_location = amxd_object_get_value(cstring_t, rlyeh, RLYEH_STORAGE_LOCATION, NULL);
    image_location = amxd_object_get_value(cstring_t, rlyeh, RLYEH_IMAGE_LOCATION, NULL);
    when_null(storage_location, error);
    when_null(image_location, error);
    amxd_object_t* images = amxd_dm_findf(rlyeh_get_dm(), RLYEH_DM_IMAGES);
    amxd_object_for_each(instance, it, images) {
        amxd_object_t* instance = amxc_container_of(it, amxd_object_t, it);
        // Check if marked remove
        bool remove = amxd_object_get_value(bool, instance, RLYEH_DM_IMAGE_MARK_RM, NULL);
        if(remove) {
            char* name = amxd_object_get_value(cstring_t, instance, RLYEH_DM_IMAGE_NAME, NULL);
            char* version = amxd_object_get_value(cstring_t, instance, RLYEH_DM_IMAGE_VERSION, NULL);
            res = remove_image(storage_location, image_location, instance);
            if(res != 0) {
                SAH_TRACEZ_ERROR(ME, "Failed removing image in gc (%d)", res);
                NOTIF_WORKER_FAILED(obj, NULL, tr181_fault_request_denied, RLYEH_CMD_GC, "error executing [delete (%s:%s)], error_code : %d", name, version, res);
                status |= tr181_fault_request_denied;
            } else {
                SAH_TRACEZ_INFO(ME, "Image removed: %s:%s", name, version);
            }
            free(name);
            free(version);
        }
    }

    // remove image spec which are not listed in Rlyeh's DM
    res = remove_unlisted_imagespec(storage_location, image_location, images);
    if(res != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed removing unlisted imagespec in gc (%d)", res);
        NOTIF_WORKER_FAILED(obj, NULL, tr181_fault_request_denied, RLYEH_CMD_GC, "error removing unlisted imagespec, error_code : %d", res);
        status |= tr181_fault_request_denied;
    }

    // remove blobs which are not listed in the image spec(s)
    res = rlyeh_remove_unlisted_blobs(storage_location, image_location);
    if(res != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed removing unlisted blobs in gc (%d)", res);
        NOTIF_WORKER_FAILED(obj, NULL, tr181_fault_request_denied, RLYEH_CMD_GC, "error removing unlisted blobs, error_code : %d", res);
        status |= tr181_fault_request_denied;
    }

    // Notify the main thread to do DM trans and external notifications.
    worker_write_notification(images, args, rlyeh_image_removed_cb);
    goto exit;
error:
    rlyeh_worker_signal_task_complete();


exit:
    free(storage_location);
    free(image_location);
    return status;
}
