/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <dirent.h>

#include <debug/sahtrace.h>
#include <yajl/yajl_gen.h>
#include <yajl/yajl_parse.h>
#include <amxc/amxc_string.h>
#include <amxc/amxc_variant_type.h>

#include <rlyeh/rlyeh_defines.h>
#include <rlyeh/rlyeh_image_info.h>
#include <rlyeh/rlyeh_utils.h>
#include <rlyeh/rlyeh_copy.h>
#include <rlyeh/rlyeh_remove.h>
#include <rlyeh/rlyeh_status.h>
#include <rlyeh/rlyeh_sync.h>
#include <rlyeh/rlyeh_image_index_generation.h>

#include "rlyeh.h"
#include "rlyeh_dm.h"
#include "rlyeh_dm_notif.h"
#include "rlyeh_worker.h"
#include "rlyeh_worker_func.h"
#include "rlyeh_common.h"
#include "rlyeh_worker_signals.h"

#define ME "rlyeh_functions"

static amxd_status_t create_image_dm(const char* duid, const char* uri, rlyeh_image_info_t* info, bool markforremoval) {
    amxd_status_t status = amxd_status_unknown_error;
    amxd_object_t* images = amxd_dm_findf(rlyeh_get_dm(), RLYEH_DM_IMAGES);
    amxd_trans_t trans;

    if(duid == NULL) {
        SAH_TRACEZ_ERROR(ME, "No DUID provided");
        status = amxd_status_invalid_arg;
        goto exit;
    }

    if(info->version == NULL) {
        SAH_TRACEZ_ERROR(ME, "No version provided");
        status = amxd_status_invalid_arg;
        goto exit;
    }

    amxd_trans_init(&trans);

    amxd_trans_select_object(&trans, images);
    amxd_trans_add_inst(&trans, 0, NULL);
    amxd_trans_set_value(cstring_t, &trans, RLYEH_DM_IMAGE_DUID, duid);
    amxd_trans_set_value(cstring_t, &trans, RLYEH_DM_IMAGE_VERSION, info->version);
    if(uri) {
        amxc_string_t str_uri;
        amxc_string_init(&str_uri, 0);

        complete_uri(uri, &str_uri);

        amxd_trans_set_value(cstring_t, &trans, RLYEH_DM_IMAGE_URI, str_uri.buffer);

        amxc_string_clean(&str_uri);
    }
    if(info->name) {
        amxd_trans_set_value(cstring_t, &trans, RLYEH_DM_IMAGE_NAME, info->name);
    }
    if(info->disklocation) {
        amxd_trans_set_value(cstring_t, &trans, RLYEH_DM_IMAGE_DISKLOCATION, info->disklocation);
    }
    if(info->vendor) {
        amxd_trans_set_value(cstring_t, &trans, RLYEH_DM_IMAGE_VENDOR, info->vendor);
    }
    if(info->description) {
        amxd_trans_set_value(cstring_t, &trans, RLYEH_DM_IMAGE_DESCRIPTION, info->description);
    }

    amxd_trans_set_value(bool, &trans, RLYEH_DM_IMAGE_MARK_RM, markforremoval);
    amxd_trans_set_value(cstring_t, &trans, RLYEH_DM_IMAGE_STATUS, RLYEH_STATUS_DOWNLOADED);
    amxd_trans_set_value(uint32_t, &trans, RLYEH_DM_IMAGE_ERROR_CODE, 0);

    status = amxd_trans_apply(&trans, rlyeh_get_dm());

    amxd_trans_clean(&trans);
exit:
    return status;
}

static amxd_status_t update_image_dm(amxd_object_t* image, const char* uri, const char* name, const char* vendor, const char* description, bool markforremoval) {
    amxd_status_t status = amxd_status_unknown_error;
    amxd_trans_t trans;
    amxd_trans_init(&trans);

    amxd_trans_select_object(&trans, image);
    if(uri) {
        amxc_string_t str_uri;
        amxc_string_init(&str_uri, 0);

        complete_uri(uri, &str_uri);

        amxd_trans_set_value(cstring_t, &trans, RLYEH_DM_IMAGE_URI, str_uri.buffer);

        amxc_string_clean(&str_uri);
    }
    if(name) {
        amxd_trans_set_value(cstring_t, &trans, RLYEH_DM_IMAGE_NAME, name);
    }
    if(vendor) {
        amxd_trans_set_value(cstring_t, &trans, RLYEH_DM_IMAGE_VENDOR, vendor);
    }
    if(description) {
        amxd_trans_set_value(cstring_t, &trans, RLYEH_DM_IMAGE_DESCRIPTION, description);
    }

    amxd_trans_set_value(bool, &trans, RLYEH_DM_IMAGE_MARK_RM, markforremoval);
    amxd_trans_set_value(cstring_t, &trans, RLYEH_DM_IMAGE_STATUS, RLYEH_STATUS_DOWNLOADED);
    amxd_trans_set_value(uint32_t, &trans, RLYEH_DM_IMAGE_ERROR_CODE, 0);

    status = amxd_trans_apply(&trans, rlyeh_get_dm());

    amxd_trans_clean(&trans);

    return status;
}

static amxd_status_t rlyeh_sync_image(const char* image_location, const char* storage_location, const char* dirname) {
    amxd_status_t status = amxd_status_ok;
    rlyeh_sync_data_t data;
    init_rlyeh_sync_data(&data);
    rlyeh_sync(image_location, storage_location, dirname, &data);
    for(size_t it = 0; it < data.len; it++) {
        if(!data.images[it]) {
            continue;
        }
        // fprintf(stderr, "Image : [%s][%s][%s][%s][%s][%s][%d]\n", data.images[it]->uri,
        //                                                         data.images[it]->duid,
        //                                                         data.images[it]->name,
        //                                                         data.images[it]->version,
        //                                                         data.images[it]->vendor,
        //                                                         data.images[it]->description,
        //                                                         data.images[it]->markforremoval);
        status = create_image_dm(data.images[it]->duid,
                                 data.images[it]->uri,
                                 &data.images[it]->image_info,
                                 data.images[it]->markforremoval);
        if(status != amxd_status_ok) {
            goto exit;
        }
    }
exit:
    free_rlyeh_sync_data(&data);
    return status;
}

static amxd_status_t recursive_sync(const char* imageLocation, const char* storageLocation, const char* dirname) {
    amxd_status_t status = amxd_status_ok;
    struct dirent* de;
    amxc_string_t dname;
    amxc_string_init(&dname, 0);
    amxc_string_setf(&dname, "%s/%s", imageLocation, dirname);
    DIR* dr = opendir(dname.buffer);
    if(dr != NULL) {
        while((de = readdir(dr)) != NULL) {
            if((strcmp(de->d_name, ".") == 0) || (strcmp(de->d_name, "..") == 0)) {
                continue;
            }
            if(de->d_type == DT_DIR) {
                amxc_string_t iname;
                amxc_string_init(&iname, 0);
                amxc_string_setf(&iname, "%s/%s", dirname, de->d_name);
                status = recursive_sync(imageLocation, storageLocation, iname.buffer);
                amxc_string_clean(&iname);
                if(status != amxd_status_ok) {
                    break;
                }
            } else {
                status = rlyeh_sync_image(imageLocation, storageLocation, dirname);
                break;
            }
        }
        closedir(dr);
    }
    amxc_string_clean(&dname);
    return status;
}

static amxd_status_t rlyeh_sync_images(void) {
    amxd_status_t status = amxd_status_unknown_error;
    amxd_object_t* rlyeh = NULL;
    char* storage_location = NULL;
    char* image_location = NULL;
    struct dirent* de = NULL;
    DIR* dr = NULL;

    rlyeh = amxd_dm_get_object(rlyeh_get_dm(), RLYEH_DM);
    when_null(rlyeh, exit);
    storage_location = amxd_object_get_value(cstring_t, rlyeh, RLYEH_STORAGE_LOCATION, NULL);
    image_location = amxd_object_get_value(cstring_t, rlyeh, RLYEH_IMAGE_LOCATION, NULL);
    when_null(storage_location, exit);
    when_null(image_location, exit);
    dr = opendir(image_location);

    // set status in case there are no entries
    status = amxd_status_ok;
    if(!dr) {
        // it is ok if the dir does not exist
        goto exit;
    }
    while((de = readdir(dr)) != NULL) {
        if((strcmp(de->d_name, ".") == 0) || (strcmp(de->d_name, "..") == 0)) {
            continue;
        }
        status = recursive_sync(image_location, storage_location, de->d_name);
        if(status != amxd_status_ok) {
            break;
        }
    }
exit:
    if(dr) {
        closedir(dr);
    }
    free(storage_location);
    free(image_location);
    return status;
}

static void rlyeh_init_dm(void) {
    amxd_status_t status = amxd_status_unknown_error;
    status = rlyeh_sync_images();
    if(status != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Failed to init Rlyeh's DM");
    }
}

amxd_status_t _Rlyeh_pull(amxd_object_t* obj,
                          UNUSED amxd_function_t* func,
                          amxc_var_t* args,
                          UNUSED amxc_var_t* ret) {
    amxd_status_t status = amxd_status_ok;
    SAH_TRACEZ_INFO(ME, "Executing Pull. URI [%s] DUID [%s] CID [%s]",
                    GET_CHAR(args, RLYEH_CMD_PULL_URI),
                    GET_CHAR(args, RLYEH_CMD_PULL_DUID),
                    GET_CHAR(args, RLYEH_NOTIF_COMMAND_ID));

    int res = rlyeh_worker_add_task(obj, args, rlyeh_exec_pull, true, false);
    if(res != 0) {
        status = amxd_status_unknown_error;
        SAH_TRACEZ_ERROR(ME, "Couldn't add task to worker's queue");
        goto exit;
    }

exit:
    return status;
}

amxd_status_t _Rlyeh_remove(UNUSED amxd_object_t* obj,
                            UNUSED amxd_function_t* func,
                            amxc_var_t* args,
                            UNUSED amxc_var_t* ret) {
    amxd_status_t status = amxd_status_ok;
    SAH_TRACEZ_INFO(ME, "Executing Remove. DUID [%s] CID [%s]",
                    GET_CHAR(args, RLYEH_CMD_PULL_DUID),
                    GET_CHAR(args, RLYEH_NOTIF_COMMAND_ID));

    int res = rlyeh_worker_add_task(obj, args, rlyeh_exec_remove, true, false);
    if(res != 0) {
        status = amxd_status_unknown_error;
        SAH_TRACEZ_ERROR(ME, "Couldn't add task to worker's queue");
        goto exit;
    }

exit:
    return status;
}

amxd_status_t _Rlyeh_gc(amxd_object_t* obj,
                        UNUSED amxd_function_t* func,
                        amxc_var_t* args,
                        UNUSED amxc_var_t* ret) {
    amxd_status_t status = amxd_status_ok;
    SAH_TRACEZ_INFO(ME, "Executing _Rlyeh_gc()");

    int res = rlyeh_worker_add_task(obj, args, rlyeh_exec_gc, true, false);
    if(res != 0) {
        status = amxd_status_unknown_error;
        SAH_TRACEZ_ERROR(ME, "Couldn't add task to worker's queue");
        goto exit;
    }

exit:
    return status;
}

amxd_status_t _Rlyeh_list(UNUSED amxd_object_t* obj,
                          UNUSED amxd_function_t* func,
                          UNUSED amxc_var_t* args,
                          UNUSED amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;
    const char* command_id = GET_CHAR(args, RLYEH_NOTIF_COMMAND_ID);

    rlyeh_notif_image_list(command_id);

    status = amxd_status_ok;
    return status;
}

amxd_status_t _Rlyeh_status(UNUSED amxd_object_t* obj,
                            UNUSED amxd_function_t* func,
                            amxc_var_t* args,
                            UNUSED amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;
    amxd_object_t* image = NULL;
    const char* command_id = GET_CHAR(args, RLYEH_NOTIF_COMMAND_ID);
    const char* duid = GET_CHAR(args, RLYEH_DM_IMAGE_DUID);
    const char* version = GET_CHAR(args, RLYEH_DM_IMAGE_VERSION);

    amxd_dm_t* dm = rlyeh_get_dm();

    if(string_is_empty(duid)) {
        status = amxd_status_invalid_arg;
        goto exit;
    }

    if(string_is_empty(version)) {
        status = amxd_status_invalid_arg;
        goto exit;
    }

    image = amxd_dm_findf(dm, RLYEH_DM_IMAGES ".[" RLYEH_DM_IMAGE_DUID " == '%s' && " RLYEH_DM_IMAGE_VERSION " == '%s'].", duid, version);
    if(image == NULL) {
        status = amxd_status_object_not_found;
        goto exit;
    }

    rlyeh_notif_image_status(image, command_id);
    status = amxd_status_ok;

exit:
    return status;
}

amxd_status_t _Rlyeh_sv(UNUSED amxd_object_t* obj,
                        UNUSED amxd_function_t* func,
                        amxc_var_t* args,
                        UNUSED amxc_var_t* ret) {
    amxd_status_t status = amxd_status_ok;
    amxd_object_t* rlyeh = amxd_dm_get_object(rlyeh_get_dm(), RLYEH_DM);
    amxd_trans_t trans;
    const bool value = GET_BOOL(args, RLYEH_CMD_SV_ENABLE);

    SAH_TRACEZ_INFO(ME, "executing SV (signature verification) (value = %d)", value);

    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, rlyeh);
    amxd_trans_set_value(bool, &trans, RLYEH_SIGNATURE_VERIFICATION, value);
    status = amxd_trans_apply(&trans, rlyeh_get_dm());
    amxd_trans_clean(&trans);
    return status;
}

/**
 * Call a generic command. Called from the DM
 *
 * @return amxd_status_t amxd_status_ok on success
 */
amxd_status_t _Rlyeh_command(UNUSED amxd_object_t* obj,
                             UNUSED amxd_function_t* func,
                             amxc_var_t* args,
                             UNUSED amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;

    const char* command = GET_CHAR(args, RLYEH_COMMAND);
    const char* command_id = GET_CHAR(args, RLYEH_NOTIF_COMMAND_ID);
    amxc_var_t* params = GET_ARG(args, RLYEH_COMMAND_PARAMETERS);
    amxc_var_t* params_new = NULL;

    if(!string_is_empty(command_id)) {
        if(params == NULL) {
            amxc_var_new(&params_new);
            amxc_var_set_type(params_new, AMXC_VAR_ID_HTABLE);
            params = params_new;
        }
        amxc_var_add_key(cstring_t, params, RLYEH_NOTIF_COMMAND_ID, command_id);
    }

    status = amxd_object_invoke_function(obj, command, params, ret);

    if(params_new) {
        amxc_var_delete(&params_new);
    }
    return status;
}

void rlyeh_pull_cb(amxd_object_t* obj, amxc_var_t* data) {
    amxd_status_t status = amxd_status_ok;
    tr181_fault_type_t cmd_status = tr181_fault_ok;
    amxd_object_t* rlyeh = NULL;
    char* storage_location = NULL;
    char* image_location = NULL;
    const char* uri = NULL;
    const char* duid = NULL;
    const char* command_id = NULL;
    rlyeh_image_info_t info;
    amxd_object_t* image = NULL;

    rlyeh_image_info_init(&info);
    when_null(obj, exit);
    when_null(data, exit);

    rlyeh = amxd_dm_get_object(rlyeh_get_dm(), RLYEH_DM);
    when_null(rlyeh, exit);
    storage_location = amxd_object_get_value(cstring_t, rlyeh, RLYEH_STORAGE_LOCATION, NULL);
    image_location = amxd_object_get_value(cstring_t, rlyeh, RLYEH_IMAGE_LOCATION, NULL);
    when_null(storage_location, exit);
    when_null(image_location, exit);
    uri = GET_CHAR(data, RLYEH_CMD_PULL_URI);
    duid = GET_CHAR(data, RLYEH_CMD_PULL_DUID);
    command_id = GET_CHAR(data, RLYEH_NOTIF_COMMAND_ID);
    cmd_status = (tr181_fault_type_t) GET_INT32(data, RLYEH_CMD_RES);
    if(cmd_status != tr181_fault_ok) {
        SAH_TRACEZ_ERROR(ME, "Pull failed for %s [%s] with error [%s]", duid, uri, tr181_fault_type_to_string(cmd_status));
        goto exit;
    }
    rlyeh_image_info_get_from_annotations(image_location, storage_location, uri, &info);

    image = amxd_dm_findf(rlyeh_get_dm(), RLYEH_DM_IMAGES ".[" RLYEH_DM_IMAGE_DUID " == '%s' && " RLYEH_DM_IMAGE_VERSION " == '%s'].", duid, info.version);
    if(image) {
        SAH_TRACEZ_INFO(ME, "update image dm");
        status = update_image_dm(image, uri, info.name, info.vendor, info.description, false);
    } else {
        SAH_TRACEZ_INFO(ME, "create image dm");
        status = create_image_dm(duid, uri, &info, false);
    }

    switch(status) {
    case amxd_status_ok:
        image = amxd_dm_findf(rlyeh_get_dm(), RLYEH_DM_IMAGES ".[" RLYEH_DM_IMAGE_DUID " == '%s' && " RLYEH_DM_IMAGE_VERSION " == '%s'].", duid, info.version);
        if(image) {
            rlyeh_notif_image_pulled(image, command_id);
        } else {
            SAH_TRACEZ_ERROR(ME, "Can't find new image [%s:%s]", duid, info.version);
        }
        break;
    case amxd_status_invalid_arg:
        NOTIF_FAILED(obj, command_id, tr181_fault_invalid_arguments, RLYEH_CMD_PULL, "%s", tr181_fault_type_to_string(tr181_fault_invalid_arguments));
        break;
    default:
        NOTIF_FAILED(obj, command_id, tr181_fault_request_denied, RLYEH_CMD_PULL, "unknown error [%d]", status);
        break;
    }

exit:
    rlyeh_worker_signal_task_complete();
    rlyeh_image_info_clean(&info);
    free(storage_location);
    free(image_location);

}

static void update_mark_for_removal(amxd_object_t* instance) {
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, instance);
    amxd_trans_set_value(bool, &trans, RLYEH_DM_IMAGE_MARK_RM, true);

    amxd_trans_apply(&trans, rlyeh_get_dm());
    amxd_trans_clean(&trans);
}

void rlyeh_remove_cb(amxd_object_t* obj, amxc_var_t* data) {
    if(!obj) {
        SAH_TRACEZ_WARNING(ME, "No data");
        goto exit;
    }
    if(!data) {
        SAH_TRACEZ_WARNING(ME, "No data");
        goto exit;
    }
    const char* command_id = GET_CHAR(data, RLYEH_NOTIF_COMMAND_ID);
    const char* duid = GET_CHAR(data, RLYEH_CMD_PULL_DUID);
    const char* version = GET_CHAR(data, RLYEH_DM_IMAGE_VERSION);
    amxd_object_t* image = amxd_dm_findf(rlyeh_get_dm(), RLYEH_DM_IMAGES ".[" RLYEH_DM_IMAGE_DUID " == \"%s\" && " RLYEH_DM_IMAGE_VERSION " == '%s'].", duid, version);
    if(image == NULL) {
        NOTIF_FAILED(obj,
                     command_id,
                     tr181_fault_request_denied,
                     RLYEH_CMD_REMOVE,
                     "Could't find image with " RLYEH_DM_IMAGE_DUID " (%s) - " RLYEH_DM_IMAGE_VERSION " (%s)",
                     duid,
                     version);
        goto exit;
    }
    SAH_TRACEZ_INFO(ME, "Mark Image for remove in DM");
    update_mark_for_removal(image);

    rlyeh_notif_image_remove_mark(image, command_id);

exit:
    return;
}

void rlyeh_image_removed_cb(amxd_object_t* obj, amxc_var_t* data) {
    if(!obj) {
        SAH_TRACEZ_WARNING(ME, "No data");
        goto exit;
    }
    if(!data) {
        SAH_TRACEZ_WARNING(ME, "No data");
        goto exit;
    }
    bool delete = false;
    amxd_object_for_each(instance, it, obj) {
        amxd_object_t* instance = amxc_container_of(it, amxd_object_t, it);
        bool remove = amxd_object_get_value(bool, instance, RLYEH_DM_IMAGE_MARK_RM, NULL);
        if(remove) {
            rlyeh_notif_image_removed(instance, GET_CHAR(data, RLYEH_NOTIF_COMMAND_ID));
            // remove instance from dm
            amxd_object_delete(&instance);
            delete = true;
        }
    }

    // Call a second time gc(), in some cases there might be some orphan image spec to remove
    if(delete) {
        amxc_var_t new_data;
        amxc_var_init(&new_data);
        if(data != NULL) {
            amxc_var_copy(&new_data, data);
        }
        int res = rlyeh_worker_add_task(obj, &new_data, rlyeh_exec_gc, true, true);
        if(res != 0) {
            SAH_TRACEZ_ERROR(ME, "Couldn't add task to worker's queue");
        }
        amxc_var_clean(&new_data);
    }
exit:
    rlyeh_worker_signal_task_complete();

    return;
}

void rlyeh_cmd_failed(amxd_object_t* obj, amxc_var_t* data) {
    if(!obj) {
        SAH_TRACEZ_WARNING(ME, "No data");
        goto exit;
    }
    if(!data) {
        SAH_TRACEZ_WARNING(ME, "No data");
        goto exit;
    }
    NOTIF_FAILED(obj, GET_CHAR(data, RLYEH_NOTIF_COMMAND_ID),
                 GET_INT32(data, RLYEH_NOTIF_ERROR_TYPE),
                 GET_CHAR(data, RLYEH_COMMAND),
                 "%s", GET_CHAR(data, RLYEH_NOTIF_ERROR_REASON));
exit:
    return;
}

static int rlyeh_execute_onboarding(amxd_object_t* rlyeh, const char* image_location, const char* storage_location) {
    int ret = 0;
    char* ro_image_location = amxd_object_get_cstring_t(rlyeh, RLYEH_IMAGE_LOCATION_RO, NULL);
    char* ro_storage_location = amxd_object_get_cstring_t(rlyeh, RLYEH_STORAGE_LOCATION_RO, NULL);

    if(!string_is_empty(ro_image_location)) {
        SAH_TRACEZ_NOTICE(ME, "Copying image files from '%s' to '%s'", ro_image_location, image_location);
        if(dir_exists(ro_image_location)) {
            ret = rlyeh_copy_dir_recursively(ro_image_location, image_location);
            if(ret) {
                SAH_TRACEZ_ERROR(ME, "Error in copying image files (%d)", ret);
            }
        } else {
            SAH_TRACEZ_WARNING(ME, RLYEH_IMAGE_LOCATION_RO " directory does not exist '%s'", ro_image_location);
        }
    } else {
        SAH_TRACEZ_WARNING(ME, "No " RLYEH_IMAGE_LOCATION_RO " defined");
    }

    if(!string_is_empty(ro_storage_location)) {
        SAH_TRACEZ_NOTICE(ME, "Symlinking storage files from '%s' to '%s'", ro_storage_location, storage_location);
        if(dir_exists(ro_storage_location)) {
            ret = rlyeh_symlink_files_in_dir_recursively(ro_storage_location, storage_location);
            if(ret) {
                SAH_TRACEZ_ERROR(ME, "Error in symlinking storage files (%d)", ret);
            }
        } else {
            SAH_TRACEZ_WARNING(ME, RLYEH_STORAGE_LOCATION_RO " directory does not exist '%s'", ro_storage_location);
        }
    } else {
        SAH_TRACEZ_WARNING(ME, "No " RLYEH_STORAGE_LOCATION_RO " defined");
    }

    free(ro_storage_location);
    free(ro_image_location);
    return ret;
}

int rlyeh_init(void) {
    int rc = -1;
    amxd_object_t* rlyeh = NULL;
    char* image_location = NULL;
    char* storage_location = NULL;
    char* onboarding_file = NULL;

    rlyeh = amxd_dm_get_object(rlyeh_get_dm(), RLYEH_DM);
    when_null(rlyeh, exit);

    image_location = amxd_object_get_cstring_t(rlyeh, RLYEH_IMAGE_LOCATION, NULL);
    storage_location = amxd_object_get_cstring_t(rlyeh, RLYEH_STORAGE_LOCATION, NULL);
    onboarding_file = amxd_object_get_cstring_t(rlyeh, RLYEH_ONBOARDING_FILE, NULL);

    if(rlyeh_mkdir(image_location, true) < 0) {
        SAH_TRACEZ_ERROR(ME, "Could not make data dir [%s]", image_location);
        goto exit;
    }
    if(rlyeh_mkdir(storage_location, true) < 0) {
        SAH_TRACEZ_ERROR(ME, "Could not make data dir [%s]", storage_location);
        goto exit;
    }

    if(!file_exists(onboarding_file)) {
        rlyeh_execute_onboarding(rlyeh, image_location, storage_location);
        if(rlyeh_create_file(onboarding_file)) {
            SAH_TRACEZ_ERROR(ME, "!!! Cannot create onboarded file !!! --> next boot we will onboard again, you probably dont want this");
        }
    }

    rlyeh_init_dm();
    rc = 0;
exit:
    free(onboarding_file);
    free(image_location);
    free(storage_location);
    return rc;

}
