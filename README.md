# R'lyeh

[[_TOC_]]

## Introduction

R'lyeh is the local repository which is used to store the manifests and blobs after they have been downloaded from the LCM Repository.  

### API

The API is defined in ODL files. Several methods are exposed to manipulate image spec:

* Pull : Download an image spec from a remote repository to the storage directory
* Remove : Set a marker on the specified image for the GC
* GC (Garbage Collector) : Remove all images that have been marked for removal

### Rlyeh.pull()  

Rlyeh.pull() needs the following arguments :

* **URI** : string mandatory
* **DUID** : string mandatory
* **username** : string optional
* **password** : string optional

R'lyeh uses services provided by librlyeh to pull the oci image spec.

Username and password are optional. If there are not specified, librlyeh retrieves them from a config file in which the credentials are specified for each server.  

Sequence diagram of Rlyeh.pull()  :  

![Rlyeh pull](rlyehpull.png)

### Rlyeh.remove()  

Rlyeh.remove() needs the following arguments :

* **paramKey** : string mandatory
* **paramValue** : string mandatory

Rlyeh.remove() marks images in the datamodel for removal. Each images with the value *paramValue* in the *paramKey* field is marked.

### Rlyeh.gc()  

Rlyeh.gc() doesn't need any argument. It removes the downloaded image marked for removal from the file system and Rlyeh's datamodel.

### Rlyeh.sv()  

Rlyeh.sv() needs the following argument :

* **Enable** : string mandatory

Rlyeh.sv() allows to enable/disable the boolean SignatureVerification in the DM.
This boolean is used during a Rlyeh.pull() to over pass the policy.json which specifies any kind of signature associated to the image.

### Datamodel  

```
> Rlyeh.Images.?
Rlyeh.Images
Rlyeh.Images.1
Rlyeh.Images.1.URI=docker://ovh-repo-01.v3d.fr:8086/zigbee_sah
Rlyeh.Images.1.Size=0
Rlyeh.Images.1.MarkForRemoval=0
Rlyeh.Images.1.Vendor=sah
Rlyeh.Images.1.Description=Zigbee container for sah
Rlyeh.Images.1.DUID=duid
Rlyeh.Images.1.ImageID=
Rlyeh.Images.1.Name=zigbee_sah
Rlyeh.Images.1.Version=latest
```

The Images section has the following parameters :
* **URI**: Remote image name used to fetch the image spec  
    Format : transport://server/imagename:version  
    *Note : server is optional, it takes the default server known by librlyeh if not specified.  
    The imagename from a docker.io registry MUST contains the group to which the image belongs.*
* **Size**: Not used
* **MarkForRemoval**: 
* **Vendor**: Annotation *org.opencontainers.image.vendor* in image spec 
* **Description**: Annotation *org.opencontainers.image.description* in image spec
* **DUID**: TR-181 parameter. Should be unique among the images.
* **ImageID**: Not Used
* **Version**: Annotation *org.opencontainers.image.version* in image spec. If there is no version specified in the image spec, the version in the URI is set.

*Note : if the version is not specified in the URI, R'lyeh fetches the "latest" version.*


### Storage of an oci image spec

OCI images that have been fetched are stored in the file system following this hierarchy :

```
ImageLocation
|
└───imagename0
│   │   oci-layout      <--- Generated file
│   │   index.json      <--- Generated file
|
└───imagename1
│   │   oci-layout      <--- Generated file
│   │   index.json      <--- Generated file
|
.
.
.
```
```
StorageLocation
|
└───sha256
    │   a3f0b948c85a...
    |   b19c222eefca...
    .
    .
    .

```

*Note : The name of each file in the StorageLocation/sha256 directory is the SHA256 hash of its content. This has can be used as the "digest" field in an OCI Content Descriptor.*

### Format of image index

The image index is a higher-level manifest which points to specific image manifests. Its content is specified by [opencontainers](https://github.com/opencontainers/image-spec/blob/main/image-index.md).

Some pre-defined annotations are used to store additional informations in the image index. Also, some additional annotations are implemented in the image index to be able to synchronize the data model with the file system at start up of R'lyeh. The format of the image index is presented bellow : 

```
{
    "schemaVersion": 2,
    "manifests": [
        {
            "mediaType": "application/vnd.oci.image.manifest.v1+json",
            "size": 544,
            "digest": "sha256:6b0e6481feebe8a351cfeebe8a9ff87cf2b0e6481f351cfeeabbe8a4d466f3d7",
            "annotations": {
                "org.opencontainers.image.ref.name": "v1.0.0",
                "annotation.softathome.image.uri": "docker://docker.v3d.fr/zigbee:v1.0.0",
                "annotation.softathome.image.duid": "01716623-5bf9-51f3-88d6-90e41dcab76d",
                "annotation.softathome.image.markforremoval": "0"
            }
        }
    ]
}
```

### Image signature verification

R'lyeh verifies image signature after each pull. To carry out this verification, it needs to retrieve the signature from a HTTP(S) server and to access to a local OpenPGP public key. The paths to the signature and the public key are available in librlyeh configuration files.  

The verification is done in three steps :  
* **Signature verification** Signature verification with a trusted OpenPGP public key.
* **Docker reference verification** Comparison between the docker reference in the JSON payload and the server name of the image to pull.
* **Manifest digest verification** Comparison between the digest contained in the signature with the digest of the pulled manifest.


## Documentation

* [OCI Image Spec](https://github.com/opencontainers/image-spec)

* Some informations about [OCI Image Layout Specification](https://github.com/opencontainers/image-spec/blob/main/image-layout.md)

* [Signing and Verifying Container Images](https://cloud.redhat.com/blog/signing-and-verifying-container-images)